﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odabir_studenta
{
    
    public class Student
    {
        //atributi klase 
        protected string ime;
        protected string prezime;
        private int brojIndeksa;
        private float prosecnaOcena;
        protected string godinaStudija;

        // Svojstva klase
        public string GodinaStudija { get { return this.godinaStudija; } }
        public string Ime { get { return this.ime; } }
        public string Prezime { get { return this.prezime; } }

        

        //Konstruktor za unosenje podataka
        public Student(string ime, string prezime, int brojIndeksa, float prosecnaOcena, string godinaStudija)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.brojIndeksa = brojIndeksa;
            this.prosecnaOcena = prosecnaOcena;
            this.godinaStudija = godinaStudija;
        }

        //Svojstvo za vracanje podatka o uspehu
        public virtual float Uspeh { get { return this.prosecnaOcena; } }

        //Metoda za ispis podataka o studentu
        public override string ToString()
        {
            return this.ime + " " + this.prezime + " " + this.Uspeh + " " + this.godinaStudija;
        }
    }
}
