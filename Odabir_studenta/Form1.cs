﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odabir_studenta
{
    public partial class FormListaStudenata : Form
    {
        //Genericka lista gde se upisuju svi uneti studenti
        List<Student> listaStudenata;
        //Inicijalizacija forme
        public FormListaStudenata()
        {
            InitializeComponent();
            listaStudenata = new List<Student>();
        }
        //Metoda koja omogucuje upotrebu odredjenih delova formu na osnovu broja elemenata u listi
        public void Dopustenje()
        {
            if(listViewStudenti.Items.Count > 0)
            {
                groupBoxPretraga.Enabled = true;
                buttonObrisi.Enabled = true;
            }
            else
            {
                groupBoxPretraga.Enabled = false;
                buttonObrisi.Enabled = false;
                
            }
            
        }
        //Otvaranje forme za dodavanje novog studenta i njegovo upisivanje
        public void Dodavanje()
        {
            UpisStudenta s = new UpisStudenta();
            s.ShowDialog();
            if (s.DialogResult == DialogResult.OK)
            {
                listaStudenata.Add(s.NoviStudent);
                listViewStudenti.Items.Add(s.NoviStudent.ToString());
                Dopustenje();
            }
        }
        //Metoda za proveru selektovanja i brisanje studenta
        public void ProveraZaBrisanje()
        {
            int br = 0;
            for (int i = 0; i < listViewStudenti.Items.Count; i++)
            {
                if (listViewStudenti.Items[i].Selected)
                {
                    if (MessageBox.Show("Da li ste sigurni?", "Oprez!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        listaStudenata.RemoveAt(i);
                        listViewStudenti.Items.RemoveAt(i);
                        MessageBox.Show("Student obrisan", "Brisanje uspesno.");
                        Dopustenje();
                        br++;
                    }
                }
            }
            if (br == 0)
            {
                MessageBox.Show("Selektujte studenta za brisanje!", "Hint!");
            }
        }
        //Metoda koja pronalazi studenta sa najboljim prosekom na odredjenoj godini
        public void Pretraga(string godina)
        {
            //Provera odabira godine
            if (listBoxGodina.SelectedIndex == -1 && !checkBoxDiplomirani.Checked)
            {
                MessageBox.Show("Izaberite godinu pre pretrage!", "Hint!");
            }
            //Dodela vrednosti ako je cekiran box za diplomiranog studenta
            if (checkBoxDiplomirani.Checked)
            {
                godina = "CETVRTA";
            }
            int index = 0;
            float najboljiProsek = 0;
            for (int i = 0; i < listaStudenata.Count; i++)
            {
                if (listaStudenata[i].GodinaStudija== godina)
                {
                     if (listaStudenata[i].Uspeh > najboljiProsek)
                    {
                        najboljiProsek = listaStudenata[i].Uspeh;
                        index = i;
                    }
                }
            }
            /*Provera u slucaju da je prvi element liste sa najboljim uspehom,
             zbog toga sto je promenjliva index ima vrednost 0 na pocetku.
            */
            if(listaStudenata[index].GodinaStudija == godina)
                MessageBox.Show(listaStudenata[index].ToString(), "Najbolji student " + "Godina " + godina);
            else 
                MessageBox.Show("Nema studenata na ovoj godini!", ":(");
        }
        //Metoda koja dodaje ili oduzima mogucnost koriscenja prilikom cekiranja checkboxa
        public void Cekiranje()
        {
            if (checkBoxDiplomirani.Checked)
            {
                listBoxGodina.Enabled = false;
            }
            else
            {
                listBoxGodina.Enabled = true;
            }
        }


       
        private void buttonDodaj_Click(object sender, EventArgs e)
        {
            Dodavanje();
        }
       
        private void checkBoxDiplomirani_CheckedChanged(object sender, EventArgs e)
        {
            Cekiranje();
        }

        private void buttonObrisi_Click(object sender, EventArgs e)
        {
            ProveraZaBrisanje();
        }

        private void buttonTrazi_Click(object sender, EventArgs e)
        {
            Pretraga(listBoxGodina.Text);
        }
    }
}
