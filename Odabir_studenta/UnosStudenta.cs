﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odabir_studenta
{
    public partial class UpisStudenta : Form
    {
        Student novistudent;
        public UpisStudenta()
        {
            InitializeComponent();
            textBoxOcenaSaDiplomskog.Enabled = false;
            
        }
        // Svojstvo koje vraca vrednost unetog studenta
        public Student NoviStudent { get { return this.novistudent; } }

        //Prikazivanje studenta nakon unosa
        public void Prikaz()
        {
            if (novistudent is DiplomiraniStudent)
                MessageBox.Show(novistudent.ToString(), "Diplomirani Student");
            else
                MessageBox.Show(novistudent.ToString(), "Student");
        }

        //Upis novog studenta
        public void Upisivanje()
        {
            //Ispitivanje da li su uneti potrebni podaci
            if (textBoxIme.Text == null || textBoxIme.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti ime studenta", "Greska!");
                return;
            }
            else if (textBoxPrezime.Text == null || textBoxPrezime.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti prezime studenta", "Greska!");
                return;
            }
            else if (textBoxbrIndexa.Text == null || textBoxbrIndexa.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti broj indexa studenta", "Greska!");
                return;
            }
            else if (textBoxprOcena.Text == null || textBoxprOcena.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti prosecnu ocenu studenta", "Greska!");
                return;
            }
            //Unos studenta
            if (!checkBoxdiplomirao.Checked)
            {
                if (listBoxGodinaStudija.SelectedIndex == -1)
                {
                    MessageBox.Show("Potrebno je izabrati godinu studije studenta", "Greska!");
                    return;
                }
                novistudent = new Student(textBoxIme.Text, textBoxPrezime.Text, Convert.ToInt32(textBoxbrIndexa.Text), Convert.ToSingle(textBoxprOcena.Text), listBoxGodinaStudija.Text);
                DialogResult = DialogResult.OK;
                Prikaz();
                Close();

            }
            //Unos diplomiranoh studenta
            else
            {
                if (textBoxOcenaSaDiplomskog.Text == null || textBoxOcenaSaDiplomskog.Text.Equals(""))
                {
                    MessageBox.Show("Potrebno je ubaciti ocenu sa diplomskog ispita", "Greska!");
                    return;
                }
                else
                {
                    novistudent = new DiplomiraniStudent(textBoxIme.Text, textBoxPrezime.Text, Convert.ToInt32(textBoxbrIndexa.Text), Convert.ToSingle(textBoxprOcena.Text), "CETVRTA", Convert.ToInt32(textBoxOcenaSaDiplomskog.Text));
                    DialogResult = DialogResult.OK;
                    Prikaz();
                    Close();
                }
            }
        }
        //Metoda koja dodaje ili oduzima mogucnost koriscenja prilikom cekiranja checkboxa
        public void Cekiranje()
        {
            if (checkBoxdiplomirao.Checked)
            {
                textBoxOcenaSaDiplomskog.Enabled = true;
                listBoxGodinaStudija.Enabled = false;
            }
            else
            {
                textBoxOcenaSaDiplomskog.Enabled = false;
                listBoxGodinaStudija.Enabled = true;
            }
            
        }
        //Zatvaranje forme klikom na dugme Otkazi
        public void Zatvaranje()
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void buttonUpisi_Click(object sender, EventArgs e)
        {
            Upisivanje();
        }

        private void checkBoxdiplomirao_CheckedChanged(object sender, EventArgs e)
        {
            Cekiranje();
        }

        private void buttonOtkazi_Click(object sender, EventArgs e)
        {
            Zatvaranje();
        }

        
    }
}
