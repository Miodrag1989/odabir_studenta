﻿namespace Odabir_studenta
{
    partial class FormListaStudenata
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxPretraga = new System.Windows.Forms.GroupBox();
            this.buttonTrazi = new System.Windows.Forms.Button();
            this.checkBoxDiplomirani = new System.Windows.Forms.CheckBox();
            this.labelGodinaStudija = new System.Windows.Forms.Label();
            this.listBoxGodina = new System.Windows.Forms.ListBox();
            this.listViewStudenti = new System.Windows.Forms.ListView();
            this.buttonDodaj = new System.Windows.Forms.Button();
            this.buttonObrisi = new System.Windows.Forms.Button();
            this.groupBoxPretraga.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Upisan skolske 2018/2019";
            // 
            // groupBoxPretraga
            // 
            this.groupBoxPretraga.Controls.Add(this.buttonTrazi);
            this.groupBoxPretraga.Controls.Add(this.checkBoxDiplomirani);
            this.groupBoxPretraga.Controls.Add(this.labelGodinaStudija);
            this.groupBoxPretraga.Controls.Add(this.listBoxGodina);
            this.groupBoxPretraga.Enabled = false;
            this.groupBoxPretraga.Location = new System.Drawing.Point(12, 248);
            this.groupBoxPretraga.Name = "groupBoxPretraga";
            this.groupBoxPretraga.Size = new System.Drawing.Size(350, 225);
            this.groupBoxPretraga.TabIndex = 1;
            this.groupBoxPretraga.TabStop = false;
            this.groupBoxPretraga.Text = "Pretraga";
            // 
            // buttonTrazi
            // 
            this.buttonTrazi.Location = new System.Drawing.Point(105, 174);
            this.buttonTrazi.Name = "buttonTrazi";
            this.buttonTrazi.Size = new System.Drawing.Size(106, 26);
            this.buttonTrazi.TabIndex = 3;
            this.buttonTrazi.Text = "Trazi";
            this.buttonTrazi.UseVisualStyleBackColor = true;
            this.buttonTrazi.Click += new System.EventHandler(this.buttonTrazi_Click);
            // 
            // checkBoxDiplomirani
            // 
            this.checkBoxDiplomirani.AutoSize = true;
            this.checkBoxDiplomirani.Location = new System.Drawing.Point(17, 145);
            this.checkBoxDiplomirani.Name = "checkBoxDiplomirani";
            this.checkBoxDiplomirani.Size = new System.Drawing.Size(77, 17);
            this.checkBoxDiplomirani.TabIndex = 2;
            this.checkBoxDiplomirani.Text = "Diplomirani";
            this.checkBoxDiplomirani.UseVisualStyleBackColor = true;
            this.checkBoxDiplomirani.CheckedChanged += new System.EventHandler(this.checkBoxDiplomirani_CheckedChanged);
            // 
            // labelGodinaStudija
            // 
            this.labelGodinaStudija.AutoSize = true;
            this.labelGodinaStudija.Location = new System.Drawing.Point(14, 33);
            this.labelGodinaStudija.Name = "labelGodinaStudija";
            this.labelGodinaStudija.Size = new System.Drawing.Size(79, 13);
            this.labelGodinaStudija.TabIndex = 1;
            this.labelGodinaStudija.Text = "Godina Studija:";
            // 
            // listBoxGodina
            // 
            this.listBoxGodina.FormattingEnabled = true;
            this.listBoxGodina.Items.AddRange(new object[] {
            "PRVA",
            "DRUGA",
            "TRECA",
            "CETVRTA"});
            this.listBoxGodina.Location = new System.Drawing.Point(17, 60);
            this.listBoxGodina.MultiColumn = true;
            this.listBoxGodina.Name = "listBoxGodina";
            this.listBoxGodina.Size = new System.Drawing.Size(179, 56);
            this.listBoxGodina.TabIndex = 0;
            // 
            // listViewStudenti
            // 
            this.listViewStudenti.Location = new System.Drawing.Point(12, 39);
            this.listViewStudenti.Name = "listViewStudenti";
            this.listViewStudenti.Size = new System.Drawing.Size(233, 184);
            this.listViewStudenti.TabIndex = 2;
            this.listViewStudenti.UseCompatibleStateImageBehavior = false;
            this.listViewStudenti.View = System.Windows.Forms.View.List;
            // 
            // buttonDodaj
            // 
            this.buttonDodaj.Location = new System.Drawing.Point(251, 51);
            this.buttonDodaj.Name = "buttonDodaj";
            this.buttonDodaj.Size = new System.Drawing.Size(97, 44);
            this.buttonDodaj.TabIndex = 3;
            this.buttonDodaj.Text = "Dodaj";
            this.buttonDodaj.UseVisualStyleBackColor = true;
            this.buttonDodaj.Click += new System.EventHandler(this.buttonDodaj_Click);
            // 
            // buttonObrisi
            // 
            this.buttonObrisi.Enabled = false;
            this.buttonObrisi.Location = new System.Drawing.Point(251, 146);
            this.buttonObrisi.Name = "buttonObrisi";
            this.buttonObrisi.Size = new System.Drawing.Size(97, 42);
            this.buttonObrisi.TabIndex = 4;
            this.buttonObrisi.Text = "Obrisi";
            this.buttonObrisi.UseVisualStyleBackColor = true;
            this.buttonObrisi.Click += new System.EventHandler(this.buttonObrisi_Click);
            // 
            // FormListaStudenata
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 496);
            this.Controls.Add(this.buttonObrisi);
            this.Controls.Add(this.buttonDodaj);
            this.Controls.Add(this.listViewStudenti);
            this.Controls.Add(this.groupBoxPretraga);
            this.Controls.Add(this.label1);
            this.Name = "FormListaStudenata";
            this.Text = "Studenti";
            this.groupBoxPretraga.ResumeLayout(false);
            this.groupBoxPretraga.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxPretraga;
        private System.Windows.Forms.Button buttonTrazi;
        private System.Windows.Forms.CheckBox checkBoxDiplomirani;
        private System.Windows.Forms.Label labelGodinaStudija;
        private System.Windows.Forms.ListBox listBoxGodina;
        private System.Windows.Forms.ListView listViewStudenti;
        private System.Windows.Forms.Button buttonDodaj;
        private System.Windows.Forms.Button buttonObrisi;
    }
}

