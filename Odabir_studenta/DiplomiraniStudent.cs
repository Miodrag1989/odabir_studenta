﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odabir_studenta
{
    class DiplomiraniStudent : Student
    {
        // atribut klase
        private int ocenaSaDiplomskog;

        //Svojstvo za cracanje i postavljanje ocene
        public int OcenaSaDiplomskog { get { return this.ocenaSaDiplomskog; } set { this.ocenaSaDiplomskog = value; } }
        
        //Konstruktor za unos podataka o dipl. studentu
        public DiplomiraniStudent(string ime, string prezime, int brojIndeksa, float prosecnaOcena, string godinaStudija, int ocenaSaDiplomskog)
        : base(ime, prezime, brojIndeksa, prosecnaOcena, godinaStudija)
        {
            this.ocenaSaDiplomskog = ocenaSaDiplomskog;
        }

        //Svojstvo za racunanje i vracanje vrednosti o Uspehu dipl. studenta
        public override float Uspeh { get { return (base.Uspeh + this.ocenaSaDiplomskog) / 2; } }

        public override string ToString()
        {
            return base.ime + " " + base.prezime + " " + this.Uspeh + " " + base.godinaStudija;
        }
    }

    
}
